use std::io;
use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashMap;
use std::time::Duration;
extern crate tokio_core;
use tokio_core::net::TcpStream;
use tokio_core::reactor::{ Timeout, Interval };
extern crate tokio_tungstenite;
extern crate tungstenite;
extern crate futures;
use futures::{ Future, Stream };
use futures::unsync::mpsc;
use futures::unsync::oneshot;
extern crate url;
use std::net::ToSocketAddrs;
extern crate tokio_io;
#[macro_use]
extern crate serde_json;
use serde_json::Value;

pub struct ChannelEvent {
    pub event: String,
    pub payload: Value,
}

struct SocketState {
    topics: HashMap<String, mpsc::UnboundedSender<ChannelEvent>>,
    current_ref: u64,
    refs: HashMap<String, oneshot::Sender<Value>>,
}

#[derive(Clone)]
pub struct ConnectionHandle {
    sender: mpsc::UnboundedSender<tungstenite::Message>,
    state: Rc<RefCell<SocketState>>,
}

impl ConnectionHandle {

    pub fn send_raw(&self, message: &Value) {
        self.sender.send(tungstenite::Message::text(
            serde_json::to_string(message).unwrap())).unwrap()
    }

    pub fn call(&self, topic: &str, event: &str, value: Value) -> oneshot::Receiver<Value> {
        let (sender, receiver) = oneshot::channel();
        let reference;
        {
            let mut borrow = self.state.borrow_mut();
            reference = borrow.current_ref;
            borrow.current_ref += 1;
            borrow.refs.insert(reference.to_string(), sender);
        }
        self.send_raw(&json!({
            "topic": topic,
            "event": event,
            "payload": value,
            "ref": reference.to_string(),
        }));
        receiver
    }

    pub fn join_channel(&self, topic: &str, payload: Value) -> Box<Future<Item = (Value, mpsc::UnboundedReceiver<ChannelEvent>), Error = ()>> {
        let (sender, receiver) = mpsc::unbounded();
        self.state.borrow_mut().topics.insert(topic.to_string(), sender);
        Box::new(self.call(topic, "phx_join", payload)
                 .and_then(|response| Ok((response, receiver)))
                 .map_err(|_| ()))
    }

}

pub fn connect<U>(req: U, handle: tokio_core::reactor::Handle)
                     -> Box<Future<Item = ConnectionHandle, Error = io::Error>>
    where U: Into<url::Url>
{

    let url: url::Url = req.into();
    let addr = url.to_socket_addrs().unwrap().next().unwrap();

    Box::new(TcpStream::connect(&addr, &handle).and_then(move |tcp| {
        tokio_tungstenite::client_async(url, tcp).and_then(move |ws_stream| {
            let (ws_sender, ws_receiver) = ws_stream.split();

            // Forward output data
            let (out_sender, out_receiver) = mpsc::unbounded();
            {
                let fut = out_receiver
                    .map_err::<tungstenite::Error, _>(|_| unreachable!())
                    .forward(ws_sender)
                    .map(|_| ())
                    .map_err(|_| panic!());
                handle.spawn(fut);
            }

            // Internal socket state
            let state = Rc::new(RefCell::new(SocketState {
                topics: HashMap::new(),
                current_ref: 0,
                refs: HashMap::new(),
            }));

            // Handle incoming messages
            {
                let state = state.clone();
                let network_handler = ws_receiver.for_each(move |message| {
                    let message_str = message.into_text().unwrap();
                    let message_val: Value =
                        serde_json::from_str(&message_str).unwrap();

                    if let Some(string) = message_val["ref"].as_str() {
                        if let Some(handler) = state.borrow_mut().refs.remove(string) {
                            handler.send(message_val["payload"].clone()).unwrap();
                            return Ok(());
                        } else {
                            panic!("received reply to nonexistent ref");
                        }
                    }

                    let topic = message_val["topic"].as_str().unwrap().to_string();
                    if let Some(val) = state.borrow().topics.get(&topic) {
                        val.send(ChannelEvent {
                            event: message_val["event"].as_str().unwrap().to_string(),
                            payload: message_val["payload"].clone(),
                        }).unwrap();
                    }

                    Ok(())
                }).map_err(|_| unimplemented!());
                handle.spawn(network_handler);
            }

            let connection_handle = ConnectionHandle {
                sender: out_sender,
                state: state.clone(),
            };

            // Heartbeat
            {
                let duration = std::time::Duration::new(2, 0);

                let connection_handle = connection_handle.clone();
                let inner_handle = handle.clone();

                let interval = Interval::new(duration, &handle)
                    .unwrap()
                    .for_each(move |_| {

                        let timeout = Timeout::new(Duration::from_secs(10), &inner_handle)
                            .unwrap();

                        connection_handle
                            .call("phoenix", "heartbeat", json!({}))
                            .select2(timeout)
                            .then(|result| {
                                use futures::future::Either;

                                match result {
                                    Ok(Either::A(_)) => println!("Got response from server"),
                                    Ok(Either::B(_)) => println!("Timed out"),
                                    _ => unimplemented!(),
                                }

                                Ok(())
                            })
                    })
                    .map_err(|_| unreachable!());
                handle.spawn(interval);
            }

            Ok(connection_handle)
        }).map_err(|e| {
            io::Error::new(io::ErrorKind::Other, e)
        })
    }))
}
