extern crate phoenix_tokio_test;
use phoenix_tokio_test::ChannelEvent;

extern crate futures;
use futures::Stream;
extern crate tokio_core;
extern crate url;
#[macro_use]
extern crate serde_json;
use serde_json::Value;

use tokio_core::reactor::Core;
use futures::{ Future, IntoFuture };

use std::env;

fn handler(value: ChannelEvent) -> Box<Future<Item = (), Error = ()>> {
    println!("{:?}: {:?}", value.event, value.payload);
    Box::new(futures::future::ok(()))
}

fn main() {

    let connect_addr = env::args().nth(1).unwrap_or_else(|| {
        panic!("this program requires at least one argument")
    });

    let url = url::Url::parse(&connect_addr).unwrap();

    let mut core = Core::new().unwrap();
    let handle = core.handle();

    let socket_future = phoenix_tokio_test::connect(url, handle.clone())
        .and_then(|connection_handle| {
            let join_1 = connection_handle
                .join_channel("rooms:lobby", json!({}))
                .and_then(|(response, receiver)| {
                    receiver.for_each(handler)
                })
                .map_err(|_| std::io::Error::new(std::io::ErrorKind::Other, ""));

            join_1
        }).map_err(|err| {
            println!("Error: {:?}", err);
            ()
        });
    handle.spawn(socket_future);

    core.run(futures::empty::<(), ()>());

}
